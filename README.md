# Get started ! #

*For any help, email me at thomaskowalski [at] outlook [dot] com, thanks !*

Hey !
How to use this ? Well, download these two files and put them in the same directory, and it should work ! It doesn't ? Contact me at thomaskowalski [at] outlook dot com or open a bug report [here](https://bitbucket.org/ThomasKowalski/icustomradio/issues/new) (preferred, since it's easier to handle) !
Files to download :

* [iTunes Interop Library](https://bitbucket.org/ThomasKowalski/icustomradio/raw/master/GTA5iTunesPlaylist/bin/Debug/Interop.iTunesLib.dll)

* [Application](https://bitbucket.org/ThomasKowalski/icustomradio/raw/master/GTA5iTunesPlaylist/bin/Debug/iCustomRadio.exe)

# Quick start #

1. In iTunes, create a playlist with the tracks you want to export to GTA.
2. In iCustomRadio, select the playlist you want to export from the list on the left. The songs it contains will be listed on the list on the right.
3. Simply click the "Export to GTA V" button.

# FAQ #
## How to get updates ? ##
Simply click on "Check updates" link. It will check for new versions and propose you to download it if there's any. Don't forget to move the file to the location of the iTunes DLL (Interop.iTunesLib.dll) if you update !

## How does this software work ? ##
It will simply "ask" iTunes where the media files are located. Then, it will create shortcuts to these files in your "User Music" folder.

## Why shortcuts ? ##
Because copying files is slow, and it takes a lot of space on your hard drive. Shortcuts are quicker, and barely 2kB.

## After using this, my GTA V is really slow ##
This comes from GTA V. Try reducing the number of tracks you have in your playlist. The more songs you have, the slower GTA will be.

## If I use an Intelligent Playlist, will my Custom Radio be updated automatically ? ##
No, you will have to start iCustomRadio again and ask it to export your playlist to GTA V again.

## GTA doesn't find any track for Custom Radio ##
This is not a question, but try changing the path to User Music in iCustomRadio parameters to another, please refer to Rockstar for this information.

## If I use this, will the tracks I already have on Custom Radio be conserved ? ##
No. Before doing anything, iCustomRadio removes everything in the User Music folder.

## It stops working at launch ##
Please download the iTunes Interop Library in the first section and place it in the same directory than the app.

## It won't launch when I launch it ##
Verify iTunes is launched. If it still doesn't work with iTunes open, contact me.