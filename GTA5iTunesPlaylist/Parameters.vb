﻿Public Class Parameters

    Private Sub Parameters_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        txtPath.Text = MainForm.UserMusicPath
    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        If txtPath.Text = "" Then
            MessageBox.Show("Please enter a path.", "No path", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End If

        If txtPath.Text.EndsWith("\") = False And txtPath.Text.EndsWith("/") = False Then
            txtPath.Text &= "\"
        End If
        If My.Computer.FileSystem.DirectoryExists(txtPath.Text) = False Then My.Computer.FileSystem.CreateDirectory(txtPath.Text)
        My.Computer.Registry.SetValue("HKEY_CURRENT_USER\Software\GTAViTunes", "Path", txtPath.Text)
        MainForm.UserMusicPath = txtPath.Text
        Me.Close()
    End Sub
End Class