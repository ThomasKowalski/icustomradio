﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' Les informations générales relatives à un assembly dépendent de 
' l'ensemble d'attributs suivant. Changez les valeurs de ces attributs pour modifier les informations
' associées à un assembly.

' Passez en revue les valeurs des attributs de l'assembly

<Assembly: AssemblyTitle("iCustomRadio")> 
<Assembly: AssemblyDescription("Export your iTunes playlists to GTA V!")> 
<Assembly: AssemblyCompany("Thomas Kowalski")> 
<Assembly: AssemblyProduct("iCustomRadio")> 
<Assembly: AssemblyCopyright("Copyright © Thomas Kowalski 2016")> 
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(False)>

'Le GUID suivant est pour l'ID de la typelib si ce projet est exposé à COM
<Assembly: Guid("19a2852b-8e21-43a8-9f50-03cb4561c133")> 

' Les informations de version pour un assembly se composent des quatre valeurs suivantes :
'
'      Version principale
'      Version secondaire 
'      Numéro de build
'      Révision
'
' Vous pouvez spécifier toutes les valeurs ou indiquer les numéros de build et de révision par défaut 
' en utilisant '*', comme indiqué ci-dessous :
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("1.0.11.0")> 
<Assembly: AssemblyFileVersion("1.0.11.0")> 
