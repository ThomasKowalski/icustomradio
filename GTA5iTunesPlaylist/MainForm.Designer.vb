﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MainForm
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(MainForm))
        Me.ListPlaylists = New System.Windows.Forms.ListBox()
        Me.ListSongs = New System.Windows.Forms.ListBox()
        Me.btnExport = New System.Windows.Forms.Button()
        Me.lnkParameters = New System.Windows.Forms.LinkLabel()
        Me.lnkHelp = New System.Windows.Forms.LinkLabel()
        Me.lnkUpdates = New System.Windows.Forms.LinkLabel()
        Me.workerUpdates = New System.ComponentModel.BackgroundWorker()
        Me.lblTracks = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'ListPlaylists
        '
        Me.ListPlaylists.FormattingEnabled = True
        Me.ListPlaylists.Location = New System.Drawing.Point(12, 12)
        Me.ListPlaylists.Name = "ListPlaylists"
        Me.ListPlaylists.Size = New System.Drawing.Size(203, 238)
        Me.ListPlaylists.TabIndex = 0
        '
        'ListSongs
        '
        Me.ListSongs.FormattingEnabled = True
        Me.ListSongs.Location = New System.Drawing.Point(221, 12)
        Me.ListSongs.Name = "ListSongs"
        Me.ListSongs.Size = New System.Drawing.Size(203, 238)
        Me.ListSongs.TabIndex = 0
        '
        'btnExport
        '
        Me.btnExport.Location = New System.Drawing.Point(12, 256)
        Me.btnExport.Name = "btnExport"
        Me.btnExport.Size = New System.Drawing.Size(203, 46)
        Me.btnExport.TabIndex = 1
        Me.btnExport.Text = "Export to GTA V"
        Me.btnExport.UseVisualStyleBackColor = True
        '
        'lnkParameters
        '
        Me.lnkParameters.AutoSize = True
        Me.lnkParameters.Location = New System.Drawing.Point(364, 289)
        Me.lnkParameters.Name = "lnkParameters"
        Me.lnkParameters.Size = New System.Drawing.Size(60, 13)
        Me.lnkParameters.TabIndex = 2
        Me.lnkParameters.TabStop = True
        Me.lnkParameters.Text = "Parameters"
        '
        'lnkHelp
        '
        Me.lnkHelp.AutoSize = True
        Me.lnkHelp.Location = New System.Drawing.Point(221, 289)
        Me.lnkHelp.Name = "lnkHelp"
        Me.lnkHelp.Size = New System.Drawing.Size(29, 13)
        Me.lnkHelp.TabIndex = 2
        Me.lnkHelp.TabStop = True
        Me.lnkHelp.Text = "Help"
        '
        'lnkUpdates
        '
        Me.lnkUpdates.AutoSize = True
        Me.lnkUpdates.Location = New System.Drawing.Point(268, 289)
        Me.lnkUpdates.Name = "lnkUpdates"
        Me.lnkUpdates.Size = New System.Drawing.Size(81, 13)
        Me.lnkUpdates.TabIndex = 3
        Me.lnkUpdates.TabStop = True
        Me.lnkUpdates.Text = "Check Updates"
        '
        'workerUpdates
        '
        '
        'lblTracks
        '
        Me.lblTracks.AutoSize = True
        Me.lblTracks.Location = New System.Drawing.Point(221, 256)
        Me.lblTracks.Name = "lblTracks"
        Me.lblTracks.Size = New System.Drawing.Size(91, 13)
        Me.lblTracks.TabIndex = 4
        Me.lblTracks.Text = "Tracks in playlist: "
        '
        'MainForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(443, 314)
        Me.Controls.Add(Me.lblTracks)
        Me.Controls.Add(Me.lnkUpdates)
        Me.Controls.Add(Me.lnkHelp)
        Me.Controls.Add(Me.lnkParameters)
        Me.Controls.Add(Me.btnExport)
        Me.Controls.Add(Me.ListSongs)
        Me.Controls.Add(Me.ListPlaylists)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "MainForm"
        Me.Text = "iCustom Radio"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ListPlaylists As System.Windows.Forms.ListBox
    Friend WithEvents ListSongs As System.Windows.Forms.ListBox
    Friend WithEvents btnExport As System.Windows.Forms.Button
    Friend WithEvents lnkParameters As System.Windows.Forms.LinkLabel
    Friend WithEvents lnkHelp As System.Windows.Forms.LinkLabel
    Friend WithEvents lnkUpdates As System.Windows.Forms.LinkLabel
    Friend WithEvents workerUpdates As System.ComponentModel.BackgroundWorker
    Friend WithEvents lblTracks As System.Windows.Forms.Label

End Class
