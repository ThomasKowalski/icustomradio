﻿Imports iTunesLib
Imports Microsoft.VisualBasic.FileIO
Imports System.Net

Public Class MainForm
    Dim iTunesController As New iTunesApp()
    Public UserMusicPath As String
    Public LastPlaylist As String

    Private Sub MainForm_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        iTunesController = Nothing 'Otherwise, iTunes will say "Hey, user, some app is using me, are you sure you want to quit ?" when closing iTunes. This will properly say "I don't want to use you anymore, iTunes !"
    End Sub

    Private Sub MainForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ListiTunesPlaylists() 'Load the playlists from iTunes and put them in the ListBox 
        LoadConfig() 'Load config (last used playlist and path)
        AddHandler workerUpdates.RunWorkerCompleted, AddressOf workerUpdates_RunWorkerCompleted 'For the updates
        If LastPlaylist <> "" Then 'We select the last playlist used, if we found it in the registry
            For Each playlist In ListPlaylists.Items
                If playlist.ToString = LastPlaylist Then
                    ListPlaylists.SetSelected(ListPlaylists.FindString(LastPlaylist), True)
                    Exit For
                End If
            Next
        End If
    End Sub

    Private Sub LoadConfig()
        If My.Computer.Registry.GetValue("HKEY_CURRENT_USER\Software\GTAViTunes\", "Path", "") = "" Then 'Path
            UserMusicPath = SpecialDirectories.MyDocuments & "\Rockstar Games\GTA V\User Music\"
            My.Computer.Registry.SetValue("HKEY_CURRENT_USER\Software\GTAViTunes\", "Path", UserMusicPath)
        Else
            UserMusicPath = My.Computer.Registry.GetValue("HKEY_CURRENT_USER\Software\GTAViTunes\", "Path", "")
        End If
        If My.Computer.Registry.GetValue("HKEY_CURRENT_USER\Software\GTAViTunes\", "LastPlaylist", "") = "" Then 'Last playlist
            LastPlaylist = ""
            My.Computer.Registry.SetValue("HKEY_CURRENT_USER\Software\GTAViTunes\", "LastPlaylist", "")
        Else
            LastPlaylist = My.Computer.Registry.GetValue("HKEY_CURRENT_USER\Software\GTAViTunes\", "LastPlaylist", "")
        End If
    End Sub

    Private Sub ListiTunesPlaylists() 'Lists the iTunes playlists
        For Each Playlist As IITPlaylist In iTunesController.LibrarySource.Playlists
            ListPlaylists.Items.Add(Playlist.Name)
        Next
    End Sub

    Private Sub ListSongsInPlaylist(Playlist As IITPlaylist) 'List the songs in the playlist passed as parameter
        ListSongs.Items.Clear()
        For Each Song As IITTrack In Playlist.Tracks
            ListSongs.Items.Add(Song.Name)
        Next
    End Sub

    Private Sub ListPlaylists_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ListPlaylists.SelectedIndexChanged
        If ListPlaylists.SelectedIndex <> -1 Then
            ListSongsInPlaylist(GetSelectedPlaylist()) 'List tracks in the playlist
            My.Computer.Registry.SetValue("HKEY_CURRENT_USER\Software\GTAViTunes\", "LastPlaylist", GetSelectedPlaylist.Name) 'Set the last used playlist
            lblTracks.Text = "Tracks in playlist: " & ListSongs.Items.Count
        End If
    End Sub

    Private Function GetSelectedPlaylist() As IITPlaylist 'Gets the selected playlist from its name
        For Each playlist As IITPlaylist In iTunesController.LibrarySource.Playlists
            If playlist.Name = ListPlaylists.SelectedItem.ToString Then
                Return playlist
            End If
        Next
        Throw New KeyNotFoundException
    End Function

    Private Function GetPlaylistTracks(Playlist As IITPlaylist) As List(Of IITFileOrCDTrack) 'Will give you a list of tracks from the playlist
        Dim returned As New List(Of IITFileOrCDTrack)
        For Each Track In Playlist.Tracks
            If My.Computer.FileSystem.FileExists(DirectCast(Track, IITFileOrCDTrack).Location) Then returned.Add(DirectCast(Track, IITFileOrCDTrack))
        Next
        Return returned
    End Function

    Private Sub CreateShortcuts(Playlist As List(Of IITFileOrCDTrack)) 'Just a loop to create shortcuts in the User Music folder
        For Each Track In Playlist
            CreateShortcut(Track.Location, UserMusicPath & "\" & IO.Path.GetFileNameWithoutExtension(DirectCast(Track, IITFileOrCDTrack).Location) & ".lnk")
        Next
    End Sub

    Private Sub CreateShortcut(File As String, Destination As String) 'Creates a shortcut to File at Destination
        Dim wsh As Object = CreateObject("WScript.Shell")
        wsh = CreateObject("WScript.Shell")
        Dim MyShortcut
        MyShortcut = wsh.CreateShortcut(Destination)
        MyShortcut.TargetPath = wsh.ExpandEnvironmentStrings(File)
        MyShortcut.WorkingDirectory = wsh.ExpandEnvironmentStrings(File)
        MyShortcut.WindowStyle = 4
        MyShortcut.Save()
    End Sub

    Private Sub btnExport_Click(sender As Object, e As EventArgs) Handles btnExport.Click
        If ListSongs.Items.Count = 0 Then
            MessageBox.Show("The playlist you selected doesn't contain any track. Please select another one.", "No track", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Exit Sub
        End If
        If GetPlaylistTracks(GetSelectedPlaylist()).Count = 0 Then
            MessageBox.Show("All the tracks contained in the playlist you selected could not be found on the disk. Please select another one. See iTunes for further details.", "Tracks not found", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Exit Sub
        End If
        If GetPlaylistTracks(GetSelectedPlaylist()).Count <> ListSongs.Items.Count Then MessageBox.Show("The playlist you selected contains tracks that could not be found on the disk. They won't be exported to GTA. See iTunes for further information.", "Tracks not found", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        If ListSongs.Items.Count > 200 Then
            Dim Result = MessageBox.Show("When using a high amount of tracks, GTA V may have an unexpected behaviour, such as lag spikes, world bugs or bugs on roads. Are you sure you want to export " & ListSongs.Items.Count & " tracks to GTA V ?", "That makes a lot of music.", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
            If Result = Windows.Forms.DialogResult.No Then Exit Sub
        End If
        My.Computer.FileSystem.DeleteDirectory(UserMusicPath, FileIO.DeleteDirectoryOption.DeleteAllContents) 'Empty the directory
        My.Computer.FileSystem.CreateDirectory(UserMusicPath) '...
        CreateShortcuts(GetPlaylistTracks(GetSelectedPlaylist())) 'Create shortcuts
        MessageBox.Show("Export complete ! You can have your songs on air on Custom Radio ! If you're already playing, don't forget to go to your GTA parameters and start a search for songs !", "Success !", MessageBoxButtons.OK, MessageBoxIcon.Information)
    End Sub

    Private Sub lnkParameters_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles lnkParameters.LinkClicked
        Parameters.ShowDialog()
    End Sub

    Private Sub lnkHelp_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles lnkHelp.LinkClicked
        Process.Start("http://bitbucket.org/thomaskowalski/icustomradio")
    End Sub

#Region "Updates"
    Dim newVersion As Boolean = False
    Dim couldCheck As Boolean = False
    Private Sub lnkUpdates_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles lnkUpdates.LinkClicked
        workerUpdates.RunWorkerAsync()
    End Sub
    Private Sub workerUpdates_DoWork(sender As Object, e As System.ComponentModel.DoWorkEventArgs) Handles workerUpdates.DoWork
        Try
            Dim updateClient As New WebClient()
            Dim lastVersion As String = updateClient.DownloadString("http://thomaskowalski.net/versions/icustomradio")
            If Application.ProductVersion <> lastVersion Then
                newVersion = True
            End If
            couldCheck = True
        Catch ex As Exception
            MessageBox.Show("We couldn't check updates. Please try again later.", "Sorry.", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub workerUpdates_RunWorkerCompleted(sender As Object, e As System.ComponentModel.RunWorkerCompletedEventArgs)
        If Not couldCheck Then Exit Sub
        If newVersion Then
            Dim download As DialogResult = MessageBox.Show("A new version was found! Download it?", "New version found!", MessageBoxButtons.YesNo, MessageBoxIcon.Information)
            If download = DialogResult.Yes Then
                Process.Start("https://bitbucket.org/ThomasKowalski/icustomradio/raw/master/GTA5iTunesPlaylist/bin/Debug/iCustomRadio.exe")
            Else
                MessageBox.Show("Ok then.", "Y U DO DIS?", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End If
        Else
            MessageBox.Show("No new version found.", "You have the last version !", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
    End Sub
#End Region
End Class
